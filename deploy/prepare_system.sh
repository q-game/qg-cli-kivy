#!/bin/bash

# Разворачиваетнеобходимую файловую структуру
# Исходные данные: каталог source c кодом из репозитория
# Выходные данные: каталоги data, cache и locals со всем необходимым содержимым
# Дальнейшие действия: настроить local_settings.py, синхронизировать СУБД и радоваться жизни

cd `dirname "$0"`;
BRANCH_ROOT="../../";
USER_NAME=$USER;

install_sys_deps(){
    cd $BRANCH_ROOT;
    echo -n "Install system deps...";
    sudo apt-get install build-essential mercurial git python2.7 \
        python-setuptools python-dev libsdl-image1.2-dev \
        libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev libsdl1.2-dev \
        libportmidi-dev libswscale-dev libavformat-dev libavcodec-dev zlib1g-dev \
        python-pygame \
        python-opengl \
        python-gst0.10 \
        python-enchant
    #echo -n "Install pygame...";
    #sudo apt-get remove --purge python-pygame
    #hg clone https://bitbucket.org/pygame/pygame
    #cd pygame
    #python2.7 setup.py build
    #sudo python2.7 setup.py install
    #cd ..
    #sudo rm -rf pygame
}

if [ "$1" ]; then
    install_sys_deps;
    echo "Creating virtual python environment...";
    cd `dirname "$0"`;
    ./env_update.sh "$1" \
        && echo "  ok" \
        || echo "  ERROR!" 1>&2;
else
   echo "Error: python version requires as first argument" 1>&2;
fi
