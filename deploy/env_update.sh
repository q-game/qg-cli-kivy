#!/bin/bash
# Удаляем старое виртуальное окружение, устанавливаем новое, накладываем патчи

cd `dirname "$0"`;
ENV_PATH='../../env/';
ROOT_PATH='../../';

sudo apt-get install libzmq3-dev

if [ $1 ]; then
    rm -r "$ENV_PATH";
    virtualenv --system-site-packages --python="python$1" "$ENV_PATH";
    $ENV_PATH/bin/pip install -r env.req;
    $ENV_PATH/bin/garden install filebrowser 

    #cd $ROOT_PATH
    #mkdir tools
    #cd tools
    #git clone http://github.com/kivy/kivy-designer/

else
   echo "Error: python version requires as first argument" 1>&2;
fi
