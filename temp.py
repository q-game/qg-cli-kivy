# пример подтягивания pub сообщений
class CliApp(App):
    use_kivy_settings = False

    def __init__(self, **kwargs):
        super(CliApp, self).__init__(**kwargs)

        self.zmq_engine_pubp = kwargs.get('zmq_engine_pubp', 'tcp://127.0.0.1:40000')

        self._zmq_context = zmq.Context()
        self._zmq_sub_socket = self._zmq_context.socket(zmq.SUB)
        self._zmq_sub_socket.setsockopt(zmq.SUBSCRIBE, "")
        self._zmq_sub_socket.connect(self.zmq_engine_pubp)

        Clock.schedule_interval(self.get_pub_message, 0.1)

    def get_pub_message(self, *args):
        try:
            request = json.loads(self._zmq_sub_socket.recv(zmq.NOBLOCK))
            print request
        except zmq.Again as e:
            #pass
            print e
