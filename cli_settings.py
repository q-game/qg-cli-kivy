#encoding: utf-8
import socket
#host = socket.gethostbyname
def host(*args):
    return '127.0.0.1'

ADRS = {
    'CLI_SRV': {
        'ip': host('qg_eng'),
        'port': '30000'
    },
    'CLI_PUB': {
        'ip': host('qg_eng'),
        'port': '40000'
    }
}
