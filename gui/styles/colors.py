from kivy.utils import get_color_from_hex as c

DARK_PRIMARY = c('#616161')
PRIMARY = c('#9E9E9E')
LIGHT_PRIMARY = c('#F5F5F5')
TEXT_ICONS = c('#212121')
ACCENT = c('#7C4DFF')
ACCENT_drk = c('#6200ea')
PRIMARY_TEXT = c('#212121')
SECONDARY_TEXT = c('#727272')
DIVIDER = c('#B6B6B6')

RED = c('#FF0000')
GREEN = c('#00CC00')
YELLOW = c('#FFFF00')
BLUE = c('#0000FF')
