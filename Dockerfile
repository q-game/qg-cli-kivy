# QGame Kivy Client
FROM ubuntu:14.04
MAINTAINER Sergey Anuchin "sergunich@gmail.com"

RUN apt-get install -y locales
RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
RUN echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen en_US.UTF-8 ru_RU.UTF-8
ENV LC_ALL en_US.UTF-8

RUN ulimit -n 1024
RUN apt-get update && apt-get install -y \
        # common
        build-essential \
        libc-dev \
        g++ \
        locales \
        mercurial \ 
        git \ 
        python2.7 \
        python-pip \
        python-setuptools  \
        python-dev \
        # kivy
        python-pygame \
        python-opengl \
        python-gst0.10 \
        python-enchant \
        gstreamer0.10-plugins-good \
        build-essential \
        libgl1-mesa-dev-lts-quantal \
        libgles2-mesa-dev-lts-quantal\
        # app
        libzmq3-dev

# python modules
RUN pip install \
    cython==0.20 \
    numpy \
    pyzmq==14.4.1 \
    ipython

ENV USE_X11 1
ENV USE_GSTREAMER 1
# TODO убрать лишние пакеты, которые тут загружаются
RUN apt-get update && apt-get install -y libx11-dev libxtst-dev x11-common libxi-dev xvfb libxrender-dev
#RUN apt-get update && apt-get install -y libgstreamer1.0-dev gstreamer-tools gstreamer0.10-alsa gstreamer0.10-buzztard gstreamer0.10-buzztard-doc gstreamer0.10-doc gstreamer0.10-fluendo-mp3 gstreamer0.10-gconf gstreamer0.10-gnomevfs gstreamer0.10-gnonlin gstreamer0.10-gnonlin-dbg gstreamer0.10-gnonlin-doc gstreamer0.10-hplugins gstreamer0.10-nice gstreamer0.10-packagekit gstreamer0.10-plugins-bad gstreamer0.10-plugins-bad-doc gstreamer0.10-plugins-base gstreamer0.10-plugins-base-apps gstreamer0.10-plugins-base-dbg gstreamer0.10-plugins-base-doc gstreamer0.10-plugins-cutter gstreamer0.10-plugins-good gstreamer0.10-plugins-good-doc gstreamer0.10-plugins-ugly gstreamer0.10-plugins-ugly-doc gstreamer0.10-pocketsphinx gstreamer0.10-pulseaudio gstreamer0.10-qapt gstreamer0.10-tools gstreamer0.10-x
RUN apt-get update && apt-get install -y libgstreamer1.0-dev gstreamer0.10-nice gstreamer0.10-plugins-bad gstreamer0.10-plugins-base gstreamer0.10-plugins-cutter gstreamer0.10-plugins-good gstreamer0.10-plugins-ugly gstreamer0.10-tools gstreamer0.10-x
RUN apt-get update && apt-get install -y gstreamer1.0-libav
RUN apt-get update && apt-get install -y libav-tools libsdl-image1.2-dev \
    libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev libsdl1.2-dev \
    libportmidi-dev libswscale-dev libavformat-dev libavcodec-dev zlib1g-dev
RUN pip install \
    kivy==1.9.0 \
    git+https://github.com/drj11/pypng@master

RUN ulimit -n 8192

RUN pip install pudb
COPY ./pudb.cfg /root/.config/pudb/pudb.cfg

VOLUME /source

WORKDIR /source/
#ENTRYPOINT ["python", "main.py"]
#CMD [""]
#
# docker run -it --rm -v $(readlink -m ./):/source:ro -v /tmp/.X11-unix:/tmp/.X11-unix:ro -e DISPLAY=unix$DISPLAY --name test_cli qg_cli






